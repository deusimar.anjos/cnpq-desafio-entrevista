# Desafio Técnico (CNPq)
Esse desafio faz parte do processo seletivo de entrevistas técnicas para o CNPq (Exclusivamente), e estão regidos pela [Licença Pública Geral GNU (GPL)](http://licencas.softwarelivre.org/gpl-3.0.pt-br.md).

**Lembre-se!**

> Será por meio deste que iremos avaliar suas habilidades técnicas. Capriche!!

> Você não é obrigado a realizar esse desafio, mas isso implicará em desclassificação automática.
 

## História
Desenvolver solução de cadastro de funcionários que permita ao administrador realizar cadastro(s) de novo(s) funcionário(s), edição (nome e descrição) e remoção lógica.

A solução também deverá possuir uma funcionalidade com acesso público (somente leitura) para visualizar os funcionários cadastrados, observando para não expor dados sensíveis que identifique os funcionários.

Módulos Esperados:
* Funcionário
    * Dados básicos
    * Endereço
    * Contato
    * Função
    * Departamento
    * Registros de Ponto

``Problemática``: Existem nesta organização funcionários que possuem mas de 25 anos de trabalho. Como será realizada uma migração de todos os registros de ponto destes funcionários ao longo de todos estes anos o desenvolvedor deve tomar medidas que ao realizar a consulta aos dados destes empregados não sobrecarregue os recursos do servidor. 

``Deve-se considerar as boas práticas aplicadas ao desenvolvimento de software, como por exemplo código limpo.``

## Objetivo
* Utilizar estilo de aquitetura REST
* Arquitetura
    * Backend (API)
        * Java >= 8
        * Spring Boot
        * JPA
            * Hibernate
        * Documentação da API (OpenAPI Specification)
    * Fronend
        * Angular >= 7

### Segurança
* Console H2 (/h2-console) com acesso público
* Listagem de funcionários
    * Visualização (Acesso Público)
    * Edição (Acesso Autenticado)
    * Cadastro (Acesso Autenticado)
    * Deleção (Acesso Autenticado)
        * O registro não deve ser deletado fisicamente

### Após Finalizar
Após concluir, você deve disponibilizar os fontes em um repositório público (GitLab, GitHub ou Bitbucket) e enviar o endereço para ``deusimar.anjos@capgemini.com``.
